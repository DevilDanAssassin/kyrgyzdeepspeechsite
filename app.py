import json

from flask import Flask, render_template, url_for, redirect, flash, request, send_from_directory, jsonify
from werkzeug.utils import secure_filename
from datetime import datetime
import os
from flask_cors import CORS
import subprocess
from os import listdir
from os.path import isfile, join

UPLOAD_FOLDER = './uploads'
TEXT_FOLDER = './donate_text/'
ALLOWED_EXTENSIONS = {'wav'}
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
lang_current = 'eng'
lang_choice = {
    'rus': 'Rus',
    'kyr': 'Kyr',
    'eng': 'Eng'
}
text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/lang', methods=['GET', 'POST'])
def lang():
    global lang_current
    if request.method == 'GET':
        return jsonify(lang_choice)
    if request.method == 'POST':
        lang_current = request.form['lang']
        return jsonify(success=True)


@app.route('/')
def index():
    return redirect('/wav')


@app.route('/wav', methods=['GET', 'POST'])
def wav():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'] + '/wav/', filename))
            file_path = '/uploads/wav/' + file.filename
            filename_txt = './uploads/text/' + file.filename + '.txt'
            f = open(filename_txt,'w')
            proc = subprocess.Popen(
                "deepspeech --model models/output_graph.pbmm --lm models/lm.binary --trie models/trie --audio ." + file_path,
                shell=True, stdout=subprocess.PIPE, )
            output = proc.communicate()[0]
            f.write(output.decode('UTF-8'))
            f.close()
            return render_template('wav.html', text=output.decode('UTF-8'), file_path=file_path, filename_txt=filename_txt)

    return render_template('wav.html')


@app.route('/donate', methods=['GET', 'POST'])
def donate():
    texts = {}
    for i in lang_choice:
        text_folder = TEXT_FOLDER + i
        text_array = []
        onlyfiles = [f for f in listdir(text_folder) if isfile(join(text_folder, f))]
        for file in onlyfiles:
            f = open(text_folder + "/" + file,'r', encoding='utf-8')
            content = f.read()
            text_array.append(content)
        texts[i] = text_array
    if request.method == 'POST':
        name = './uploads/donate/' + datetime.now().strftime("%H.%M.%S.%f-%b.%d.%Y_") + 'file_donate.wav'
        f = open(name, 'wb')
        f.write(request.get_data('audio_data'))
        f.close()
        return jsonify(success=True)
    else:
        texts = json.dumps(texts)
        return render_template('donate.html', texts=texts)


@app.route('/microphone', methods=['GET', 'POST'])
def microphone():
    if request.method == 'POST':
        filename = datetime.now().strftime("%H.%M.%S.%f-%b.%d.%Y_") + 'file_microphone.wav'
        name = './uploads/microphone/' + filename
        f = open(name, 'wb')
        f.write(request.get_data('audio_data'))
        f.close()
        filename_txt = './uploads/text/' + filename + '.txt'
        f = open(filename_txt, 'w')
        proc = subprocess.Popen(
            "deepspeech --model models/output_graph.pbmm --lm models/lm.binary --trie models/trie --audio " + name,
            shell=True, stdout=subprocess.PIPE, )
        output = proc.communicate()[0]
        f.write(output.decode('UTF-8'))
        print(output.decode('UTF-8'))
        f.close()
        return jsonify(filename_txt=filename_txt, text=output.decode('UTF-8'))
    else:
        return render_template('microphone.html')


@app.route('/uploads/<path:filename>')
def download_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename, as_attachment=True)

if __name__ == '__main__':
    app.run(debug=True)
